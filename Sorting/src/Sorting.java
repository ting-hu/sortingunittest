public class Sorting
{
    //Recursively sort
    public static void mergeSort(int[] unsortedArray, int beginning, int end)
    {
        if(beginning < end)
        {
            //Calculate middle of list
            int mid = (beginning + end)/2;

            //Recursively sort left sub list
            mergeSort(unsortedArray, beginning, mid);

            //Recursively sort right sub list
            mergeSort(unsortedArray, mid+1, end);

            //Merge the sorted left and right sub list
            merge(unsortedArray, beginning, mid, end);

        }
    }

    //Merge sorted sub lists
    private static void merge(int[] unsortedArray, int low, int someMid, int high)
    {
        //beginning of first sub list
        int spot1 = low;

        //beginning of second sub list
        int spot2 = someMid + 1;

        //index of temporary array
        int counter = 0;

        //temporary array space
        int[] temp = new int[high+1-low];

        //Loop until either the end of either sub list
        while(spot1 <= someMid && spot2 <= high)
        {

            //If the element in the first sub list is smaller
            if(unsortedArray[spot1] <= unsortedArray[spot2])
            {
                //Store element in first sub list in temprotary array
                temp[counter] = unsortedArray[spot1];

                //Increment the index in the first sub list
                spot1++;
            }
            //If the element in the second sub list is smaller
            else
            {
                //Store element in second sub list in temprotary array
                temp[counter] = unsortedArray[spot2];

                //Increment the index in the second sub list
                spot2++;
            }

            counter++;
        }

        //If there are no more elements in the first sub list
        if(spot1 > someMid)
        {
            //Copy over remaining elements from the second sub list to the temporary array
            for(int k=spot2; k<=high; k++)
            {
                temp[counter] = unsortedArray[k];
                counter++;

            }

        }
        //If there are no more elements in the second sub list
        else
        {
            //Copy over remaining elements from the first sub list to the temporary array
            for(int k=spot1; k<=someMid; k++)
            {
                temp[counter] = unsortedArray[k];
                counter++;
            }

        }

        //Copy all elements from the temporary array to the original array
        for(int k=low; k<=high; k++)
        {
            unsortedArray[k] = temp[k-low];
        }

    }


    public static void quickSort(int[] unsortedArray, int minPosition, int maxPosition)
    {
        if(minPosition >= 0 && maxPosition < unsortedArray.length &&  minPosition < maxPosition)
        {
            //Set pivot to the first element
            int pivot = unsortedArray[minPosition];

            //Start from the minimum position
            int openPosition = minPosition;

            //Set min and max
            int min = minPosition+1;
            int max = maxPosition;

            //Set which pointer to use
            boolean high = true;

            while(min <= max)
            {
                if(high)
                {
                    //Decrement max until max < pivot
                    while(unsortedArray[max] >= pivot && min <= max)
                    {
                        max--;
                    }

                    if(min <= max)
                    {
                        //Swap the position of max with the open position
                        //Swap open position with max
                        unsortedArray[openPosition] = unsortedArray[max];
                        openPosition = max;
                        max--;
                    }
                }
                else
                {
                    //Increment min until min > pivot
                    while(unsortedArray[min] < pivot && min <= max)
                    {
                        min++;
                    }

                    if(min <= max)
                    {
                        //Swap the position of min with the open position
                        //Swap open position with min
                        unsortedArray[openPosition] = unsortedArray[min];
                        openPosition = min;
                        min++;
                    }
                }

                //Switch pointers
                high = !high;
            }

            //Place the pivot element at the final open position
            unsortedArray[openPosition] = pivot;

            //Sort left sub list
            quickSort(unsortedArray, minPosition, openPosition-1);

            //Sort right sub list
            quickSort(unsortedArray, openPosition+1, maxPosition);
        }

    }
}
