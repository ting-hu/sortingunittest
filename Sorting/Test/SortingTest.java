import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SortingTest {

    @Test
    void mergeSortSuccessPositiveEven() {
        int[] actual = {2, 12, 9, 1, 6, 3};
        int[] expected = {1, 2, 3, 6, 9, 12};

        Sorting sorting = new Sorting();
        sorting.mergeSort(actual,0,actual.length-1);
        assertArrayEquals(expected,actual);
    }

    @Test
    void mergeSortIfBeginningGreaterEqualEnd() {
        int[] actual = {2, 12, 9, 1, 6, 3};
        int[] expected = {2, 12, 9, 1, 6, 3};

        Sorting sorting = new Sorting();
        sorting.mergeSort(actual,actual.length-1,0);
        assertArrayEquals(expected,actual);
    }

    @Test
    void mergeSortSuccessNegativeEven() {
        int[] actual = {-2, -12, -9, -1, -6, -3};
        int[] expected = {-12,-9, -6, -3, -2, -1};

        Sorting sorting = new Sorting();
        sorting.mergeSort(actual,0,actual.length-1);
        assertArrayEquals(expected,actual);
    }

    @Test
    void mergeSortSuccessPositiveOdd() {
        int[] actual = {2, 12, 9, 1, 6, 3, 19};
        int[] expected = {1, 2, 3, 6, 9, 12, 19};

        Sorting sorting = new Sorting();
        sorting.mergeSort(actual,0,actual.length-1);
        assertArrayEquals(expected,actual);
    }

    @Test
    void mergeSortSuccessNegativeOdd() {
        int[] actual = {-2, -12, -9, -1, -6, -15, -3};
        int[] expected = {-15, -12,-9, -6, -3, -2, -1};

        Sorting sorting = new Sorting();
        sorting.mergeSort(actual,0,actual.length-1);
        assertArrayEquals(expected,actual);
    }

    @Test
    void quickSortSuccessPositiveEven() {
        int[] actual = {2, 12, 9, 1, 6, 3};
        int[] expected = {1, 2, 3, 6, 9, 12};

        Sorting sorting = new Sorting();
        sorting.quickSort(actual,0,actual.length-1);
        assertArrayEquals(expected,actual);
    }

    @Test
    void quickSortSuccessNegativeEven() {
        int[] actual = {-2, -12, -9, -1, -6, -3};
        int[] expected = {-12,-9, -6, -3, -2, -1};

        Sorting sorting = new Sorting();
        sorting.quickSort(actual,0,actual.length-1);
        assertArrayEquals(expected,actual);
    }

    @Test
    void quickSortSuccessPositiveOdd() {
        int[] actual = {2, 12, 9, 1, 6, 3, 19};
        int[] expected = {1, 2, 3, 6, 9, 12, 19};

        Sorting sorting = new Sorting();
        sorting.quickSort(actual,0,actual.length-1);
        assertArrayEquals(expected,actual);
    }

    @Test
    void quickSortSuccessNegativeOdd() {
        int[] actual = {-2, -12, -9, -1, -6, -15, -3};
        int[] expected = {-15, -12,-9, -6, -3, -2, -1};

        Sorting sorting = new Sorting();
        sorting.quickSort(actual,0,actual.length-1);
        assertArrayEquals(expected,actual);
    }
}